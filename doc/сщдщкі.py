COLOR_SHEMA = ("R", "G", "B")

colors_name_dict = {}

while True:
    color = []
    for elem in COLOR_SHEMA:
        color_value = input(f"input {elem} (0...255): ")
        if not color_value:
            break
        if not color_value.isdigit() and (0 <= int(color_value) <= 255):
            print("value must be integer in range 0 ... 255")
            continue
    if not color_value:
        break
    color.append(int(color_value))
    color = tuple(color)
    name = input(f"input name for color {color}: ")
    colors_name_dict[color] = name

print(f"result: {colors_name_dict }")
